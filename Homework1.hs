{-# LANGUAGE ViewPatterns #-}

toDigits :: Integer -> [Integer]
toDigits 0 = []
toDigits val = toDigits(val `div` 10) ++ [val `mod` 10]

reverseList :: [Integer] -> [Integer]
reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

toDigitsRev :: Integer -> [Integer]
toDigitsRev val = reverseList (toDigits val)

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther [] = []
doubleEveryOther [x] = [x]
doubleEveryOther (reverse -> (x:y:zs)) = doubleEveryOther (reverse zs) ++ [2*y, x]

sumDigits :: [Integer] -> Integer
sumDigits [] = 0
sumDigits (x:xs) = sum (toDigits x) + sumDigits xs

validate :: Integer -> Bool
validate val = sumDigits(doubleEveryOther (toDigits val)) `mod` 2 == 0

main = do
    print (validate 4012888888881881)
    print (validate 4012888888881882)